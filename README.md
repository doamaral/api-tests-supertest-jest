# API Tests using Supertest and Jest

[![pipeline status](https://gitlab.com/doamaral/api-tests-supertest-jest/badges/master/pipeline.svg)](https://gitlab.com/doamaral/api-tests-supertest-jest/commits/master)

## Requirements
- Node v18.16.1 +
- NPM v9.8.0 +

## Libraries used
- [supertest](https://www.npmjs.com/package/supertest): provide a high-level abstraction for testing HTTP
- [jest](https://www.npmjs.com/package/jest): JavaScript Testing Library
- [dotenv](https://www.npmjs.com/package/dotenv): zero-dependency module that loads environment variables from a .env

## Project setup
- Clone the project
- Into the project folder run `npm install`
- Rename `.env.example` to `.env` and change the base URL value to `https://api.publicapis.org`

## Running tests
- Run `npm run test`

